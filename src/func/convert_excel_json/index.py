from collections import OrderedDict
import json
import codecs
import xlrd

# def get_json(path, write_path):
#     file = open(path, encoding='utf8', errors='ignore')
#     file_lines = file.read()
#     file.close()
#     file_json = json.loads(file_lines)
#     json_txt = []
#     for json_data in file_json:
#     	if isinstance(json_data, dict):
#         	print(json_data)
#         	nums = random.randint(1, 100)
#         	# 向json文件中写入新的键值对
#         	json_data["number"] = str(nums)
#         	json_txt.append(json_data)
# 	return

def excel2json():
    wb = xlrd.open_workbook('data/knowledge_base.xlsx')

    convert_list = []
    sh = wb.sheet_by_index(0)
    title = sh.row_values(0)
    for rownum in range(1, sh.nrows):
        rowvalue = sh.row_values(rownum)
        single = OrderedDict()
        for colnum in range(0, len(rowvalue)):
            print(title[colnum], rowvalue[colnum])
            single[title[colnum]] = rowvalue[colnum]
        convert_list.append(single)

    j = json.dumps(convert_list)

    with codecs.open('positive_previous.json', "w", "utf-8") as f:
        f.write(j)

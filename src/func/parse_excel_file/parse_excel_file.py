###########################
# Use a csv file to store all QA pairs for one staff.
# Each staff has her/his own QA file.
# Reference:
# 1. https://stackoverflow.com/questions/62253718/how-can-i-receive-file-in-python-telegram-bot
###########################

import pandas as pd

FILE = "src/func/parse_excel_file/test_data.xlsx"

df = pd.read_excel(FILE)
print(df)

print(df.columns)

print(df.index)

print(df["Question"])

print(df.describe())
This is Server Workers Chatbot. If you are a student, here you can ask questions and get responds without waiting for long. If you are a school staff, here you can set automatically respond and get summaries about chatting history with students.

Here you can send key words about questions you want to ask. 

setcommands:
label - get all labels of questions
question - get most frequently asked questions
summary - get summary of chat histories

from flask import Flask, request
import requests
import json
import csv

from requests.api import delete
from telebot_files.credentials import bot_token, bot_user_name, URL
import telebot
from telegram.ext import Updater, MessageHandler, Filters
import telegram
from summary import generate_summary
from auto_suggestion import match_input_with_tags
from src.func.convert_excel_json.index import excel2json

global bot
global TOKEN
TOKEN = bot_token
bot = telegram.Bot(token=TOKEN)
CORPUS = "corpus/corpus.json"
# bot = telebot.TeleBot(TOKEN)

QIDtoAnswerDcit = {}
QIDtoQuestionDict = {}
LabeltoQIDDict = {}
TagtoQIDDict = {}
global STATE
STATE = "init"
global CURRENT_QID
def createQuestionDB(corpus):
    with open(corpus, encoding='utf-8') as file:
        load_data = json.load(file)
        for item in load_data:
            if item["label"] not in LabeltoQIDDict.keys():
                LabeltoQIDDict[item["label"]] = [item["qid"]]
            else:
                LabeltoQIDDict[item["label"]].append(item["qid"])
            QIDtoAnswerDcit[item["qid"]] = item["answer"]
            QIDtoQuestionDict[item["qid"]] = item["question"]
            TagtoQIDDict[item["tag"]] = item["qid"]


app = Flask(__name__)
def sendmessage(chatid, reply):
    url = "https://api.telegram.org/bot{}/sendMessage".format(TOKEN)
    payload = {
        "text":reply,
        "chat_id":chatid
        }
    resp = requests.get(url,params=payload)
    print("send successful")


@app.route('/test', methods=['POST'])
def test():
    return "test done"

def replyLabelList():
    label_list = LabeltoQIDDict.keys()
    labels = "Here are all labels you can choose: \n"
    for item in label_list:
        labels = labels + '/' + item + '\n'
    labels += "Click a label to see corresponding questions."
    return labels

def replyQuestionList(key):
    key = key[1:]
    if key not in LabeltoQIDDict.keys():
        return "Sorry we do not have this key word. Can you try another one?"
    question = []
    qid = []
    for item in LabeltoQIDDict[key]:
        qid.append(item)
        question.append(QIDtoQuestionDict[item])
    reply = "Do you want to ask the following questions?\n"
    for i in range(len(qid)):
        reply += '/'+ str(qid[i]) + ' - ' + question[i] + "\n"
    reply += "Click a number to get the corresponding answer."
    return reply

def replyAnswer(qid):
    print(qid)
    try:
        reply = QIDtoAnswerDcit[int(qid[1:])]
    except:
        reply = "Sorry, I cannot give you a corresponding reply."
    return reply

def replyQuestionListUsingTag(tag):
    reply = "Here is the suggested question.\n"
    print(tag)
    tag = tag[1:]
    if tag not in TagtoQIDDict.keys():
        return ""
    qid = TagtoQIDDict[tag]
    question = QIDtoQuestionDict[qid]
    reply += '/'+ str(qid) + ' - ' + question + "\n"
    reply += "If you want to ask this question, please choose the index."
    return reply

def replyFreqQuestionList():
    reply = "Here are frequently asked questions.\n"
    for key in QIDtoQuestionDict.keys():
        question = QIDtoQuestionDict[key]
        reply = reply + '/'+ str(key) + ' - ' + question + "\n"
    reply += "If you want to ask one of these questions, please choose the corresponding index."
    return reply

def replyEndWords():
    reply = "Has this answer solved you question?\n"
    reply += "/Yes\n/No"
    return reply

def collectUserFeedback(txt):
    header = ['qid', 'feedback']
    with open('feedback/collection.csv', 'a+') as csvfile:
        # creating a csv writer object 
        csvwriter = csv.writer(csvfile) 
            
        # # writing the header 
        # csvwriter.writerow(header) 
            
        # writing the data rows 
        feedback = 0
        if txt[1:] == "Yes":
            feedback = 1
        qid = CURRENT_QID[1:].strip('\n')
  
        csvwriter.writerow([int(qid), feedback])
    return

# def updateCorpus(path):
#     CORPUS

def downloader(file_id):
    s = requests.get("https://api.telegram.org/bot{}/getFile?file_id={}".format(TOKEN,file_id))
    res = json.loads(s.text)
    file_path = res['result']['file_path']
    r = requests.get("https://api.telegram.org/file/bot{}/{}".format(TOKEN,file_path))
    with open("data/knowledge_base.xlsx", "wb") as f:
        f.write(r.content)
    excel2json("data/knowledge_base.xlsx", "data/knowledge_base.json")
    # updateCorpus("data/knowledge_base.json")
# delete
def auto_suggestion(word):
    return "mock\n/Scholarship \n end"

@app.route("/",methods=["POST","GET"])
def index():
    # fileId = "BQACAgUAAxkBAAIC22FOOmF0_p-jDrezdzlZSozu3zowAAJbBAAC_hpxVjO7BNedBrheIQQ"
    # downloader(fileId)
    # print('end')
    # return
    if(request.method == "POST"):
        resp = request.get_json()
        print(resp)
        if "message" in resp.keys():
            msg = "message"
        elif 'edited_message' in resp.keys():
            msg = "edited_message"
        if "document" in resp[msg].keys():
            fileId = resp[msg]["document"]["file_id"]
            print(fileId)
            downloader(fileId)
        try:
            msgtext = resp[msg]["text"]
        except:
            msgtext = ""
        sendername = resp[msg]["from"]["first_name"]
        chatid = resp[msg]["chat"]["id"]
        reply = "You can try to run /label to see suggested questions."
        global STATE
        global CURRENT_QID
        print("===============================", msgtext)
        # To reset chatbot if having bugs
        if msgtext == "/back":
            STATE = "init"
            print("====================", STATE)
            return "Hello"
        if STATE == "label":
            reply = replyQuestionList(msgtext)
            STATE = "question"
        elif STATE == "question":
            reply = replyAnswer(msgtext)
            CURRENT_QID = msgtext
            sendmessage(chatid, reply)
            STATE = "collect"
            reply = replyEndWords()
        elif STATE == "tag":
            reply = replyQuestionListUsingTag(msgtext)
            STATE = "question"
        elif STATE == "collect":
            STATE = "init"
            # To Collect Response
            collectUserFeedback(msgtext)
            reply = "Thank you for your respond. :)"
        elif msgtext == '/label':
            STATE = "label"
            reply = replyLabelList()
        elif msgtext == '/question':
            STATE = "question"
            reply = replyFreqQuestionList()
        elif msgtext == '/summary':
            reply = generate_summary()
        else:
            reply = match_input_with_tags(msgtext)
            STATE = "tag"
        sendmessage(chatid, reply)
    return "Hello"

NGROK = "1452-108-169-200-180"
# Change ngrok_url, and Run
@app.route("/setwebhook")
def setwebhook():
    ngrok_url = NGROK
    url = "https://"+ngrok_url+".ngrok.io/"
    s = requests.get("https://api.telegram.org/bot{}/setWebhook?url={}".format(TOKEN,url))
    if s:
        return "yes"
    else:
        return "fail"

if __name__ == "__main__":
    corpus = CORPUS
    createQuestionDB(corpus)
    app.run(debug=True)